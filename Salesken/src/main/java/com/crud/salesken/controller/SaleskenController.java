package com.crud.salesken.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.crud.salesken.exception.AccountInfoExistException;
import com.crud.salesken.exception.MissingAccountInfoException;
import com.crud.salesken.model.Account;
import com.crud.salesken.service.AccountServiceIntf;

@RestController
public class SaleskenController {
	@Autowired
	private AccountServiceIntf accountService;

	@PostMapping(path = "/salesken/account", consumes = "application/json", produces = "application/json")
	public ResponseEntity<Account> createAccount(@RequestBody Account account) {
		if (account.getAccountNumber() != null && accountService.getAccount(account.getAccountNumber()) != null) {
			throw new AccountInfoExistException(
					"Account is aleady exist with account number " + account.getAccountNumber());
		}
		return new ResponseEntity<Account>(accountService.createAccount(account), HttpStatus.CREATED);
	}

	@GetMapping(path = "/salesken/account")
	public List<Account> getAllAccount() {
		return accountService.getAccount();
	}

	@GetMapping(path = "/salesken/account/{accountId}")
	public Account getAccount(@PathVariable("accountId") int accountId) {
		Account account = accountService.getAccount(accountId);
		if (account == null) {
			throw new MissingAccountInfoException(
					"Account is not exist with account number " + account.getAccountNumber());
		}
		return account;
	}

	@PutMapping(path = "/salesken/account")
	public Account updateAccount(@RequestBody Account account) {
		if (account.getAccountNumber() != null && accountService.getAccount(account.getAccountNumber()) == null) {
			throw new MissingAccountInfoException(
					"Account is not exist with account number " + account.getAccountNumber());
		}
		if (account.getAccountNumber() == null) {
			throw new MissingAccountInfoException("Account number is not present in the request. ");
		}

		return accountService.createAccount(account);
	}

	@DeleteMapping(path = "/salesken/account/{accountId}")
	public void deleteAccount(@PathVariable("accountId") int accountId) {
		if (accountService.getAccount(accountId) == null) {
			throw new MissingAccountInfoException("Account is not exist with account number " + accountId);
		}
		accountService.deleteAccount(accountId);
	}

}
