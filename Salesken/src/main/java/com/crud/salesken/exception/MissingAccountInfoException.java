package com.crud.salesken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MissingAccountInfoException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MissingAccountInfoException(String message) {
		super(message);
	}
}
