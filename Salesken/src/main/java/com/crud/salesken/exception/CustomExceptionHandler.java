package com.crud.salesken.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class CustomExceptionHandler {
	@ExceptionHandler(MissingAccountInfoException.class)
	public final ResponseEntity<ErrorResponse> handleInvalidTraceIdException(MissingAccountInfoException ex,
			WebRequest request) {
		ErrorResponse error = new ErrorResponse(ex.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(AccountInfoExistException.class)
	public final ResponseEntity<ErrorResponse> handleInvalidTraceIdException(AccountInfoExistException ex,
			WebRequest request) {
		ErrorResponse error = new ErrorResponse(ex.getLocalizedMessage(), request.getDescription(false));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

}
