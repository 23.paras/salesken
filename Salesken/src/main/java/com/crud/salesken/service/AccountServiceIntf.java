package com.crud.salesken.service;

import java.util.List;

import com.crud.salesken.model.Account;

public interface AccountServiceIntf {

	public Account createAccount(Account account);

	public List<Account> getAccount();

	public Account getAccount(int accountId);

	public void deleteAccount(int accountId);

}
