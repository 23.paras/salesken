package com.crud.salesken.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crud.salesken.model.Account;
import com.crud.salesken.repository.AccountRepository;

@Component
public class AccountService implements AccountServiceIntf {
	@Autowired
	private AccountRepository accountRepository;

	@Override
	public Account createAccount(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public List<Account> getAccount() {
		return accountRepository.findAll();
	}

	@Override
	public Account getAccount(int accountId) {
		return accountRepository.findById(accountId).orElse(null);
	}

	@Override
	public void deleteAccount(int accountId) {
		accountRepository.deleteById(accountId);

	}

}
