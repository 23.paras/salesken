package com.crud.salesken.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.salesken.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
}
