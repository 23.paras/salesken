package com.crud.Salesken;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.crud.salesken.controller.SaleskenController;
import com.crud.salesken.model.Account;
import com.crud.salesken.service.AccountServiceIntf;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(SaleskenController.class)
public class SaleskenControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AccountServiceIntf accountService;

	@Test
	public void createAccount_test() throws Exception {
		when(accountService.createAccount(new Account(null, "paras", 100))).thenReturn(new Account(1, "paras", 100));

		Account input = new Account(null, "paras", 100);
		RequestBuilder request = MockMvcRequestBuilders.post("/salesken/account")
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(input));
		MvcResult result = mockMvc.perform(request).andExpect(status().isCreated()).andReturn();

	}

	@Test
	public void createAccount_negative_test() throws Exception {
		when(accountService.createAccount(new Account(null, "paras", 100))).thenReturn(new Account(1, "paras", 100));
		when(accountService.getAccount(1)).thenReturn(new Account(1, "paras", 100));
		Account input = new Account(1, "paras", 100);
		RequestBuilder request = MockMvcRequestBuilders.post("/salesken/account")
				.contentType(MediaType.APPLICATION_JSON).content(new ObjectMapper().writeValueAsString(input));
		MvcResult result = mockMvc.perform(request).andExpect(status().isBadRequest()).andReturn();

	}

	@Test
	public void getAccount_test() throws Exception {
		when(accountService.getAccount(1)).thenReturn(new Account(1, "paras", 100));

		RequestBuilder request = MockMvcRequestBuilders.get("/salesken/account/1");
		MvcResult result = mockMvc.perform(request).andExpect(status().isOk())
				.andExpect(content().json("{\"accountNumber\":1,\"name\":\"paras\",\"balance\":100}")).andReturn();
	}

	@Test
	public void updateAccount__test() throws Exception {
		when(accountService.createAccount(new Account(1, "paras", 100))).thenReturn(new Account(1, "paras", 100));
		when(accountService.getAccount(1)).thenReturn(new Account(1, "paras", 100));
		Account input = new Account(1, "paras", 100);
		RequestBuilder request = MockMvcRequestBuilders.put("/salesken/account").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(input));
		MvcResult result = mockMvc.perform(request).andExpect(status().isOk()).andReturn();

	}

	@Test
	public void updateAccount_negative_test() throws Exception {

		when(accountService.getAccount(1)).thenReturn(null);
		Account input = new Account(1, "paras", 100);
		RequestBuilder request = MockMvcRequestBuilders.put("/salesken/account").contentType(MediaType.APPLICATION_JSON)
				.content(new ObjectMapper().writeValueAsString(input));
		MvcResult result = mockMvc.perform(request).andExpect(status().isBadRequest()).andReturn();

	}
}
